var express = require('express');
var Drone = require('../../../');

var app = express();
var ACTIVE = true;
var STEPS = 2;
var d = new Drone(process.env.UUID);

function cooldown() {
	ACTIVE = false;
	setTimeout(function () {
		ACTIVE = true;
	}, STEPS * 12);
}

if (process.env.UUID) {
	console.log('Searching for ', process.env.UUID);
}

// init/end
app.get('/emergency', function (req, res) {
	d.emergency();
	setTimeout(function () {
		process.exit();
	}, 3000);
	res.send('Emergency landing...');
});

app.get('/takeoff', function (req, res) {
	d.takeOff();
	res.send('Ready for take off...');
});

app.get('/landing', function (req, res) {
	d.land();
	res.send('Prepare for landing...');
});

// navigations
app.get('/up', function (req, res) {
	d.up({ steps: STEPS * 2.5 });
	cooldown();
	res.send('Going UP!');
});

app.get('/left', function (req, res) {
	d.turnLeft({ steps: STEPS });
	cooldown();
	res.send('Going LEFT!');
});

app.get('/right', function (req, res) {
	d.turnRight({ steps: STEPS });
	cooldown();
	res.send('Going RIGHT!');
});

app.get('/down', function (req, res) {
	d.down({ steps: STEPS * 2.5 });
	cooldown();
	res.send('Going DOWN!');
});

// flight controls
app.get('/forward', function (req, res) {
	d.forward({ steps: STEPS });
	cooldown();
	res.send('Moving FORWARD!');
});

app.get('/tiltleft', function (req, res) {
	d.tiltLeft({ steps: STEPS });
	cooldown();
	res.send('Tilt LEFT!');
});

app.get('/tiltright', function (req, res) {
	d.tiltRight({ steps: STEPS });
	cooldown();
	res.send('Tilt RIGHT!');
});

app.get('/backward', function (req, res) {
	d.backward({ steps: STEPS });
	cooldown();
	res.send('Going BACKWARD!');
});

// flippers
app.get('/flipfront', function (req, res) {
	d.frontFlip({ steps: STEPS });
	cooldown();
	res.send('Round we go to the FRONT!');
});

app.get('/flipleft', function (req, res) {
	d.leftFlip({ steps: STEPS });
	cooldown();
	res.send('Round we go to the LEFT!');
});

app.get('/flipright', function (req, res) {
	d.rightFlip({ steps: STEPS });
	cooldown();
	res.send('Round we go to the RIGHT!');
});

app.get('/flipback', function (req, res) {
	d.backFlip({ steps: STEPS });
	cooldown();
	res.send('Round we go to the BACK!');
});

/**
 * Connect to any available drone and start server endpoints
 * 
 * @param  {} function(
 */
d.connect(function () {
	d.setup(function () {
		console.log('Configured for Rolling Spider! ', d.name);
		d.flatTrim();
		d.startPing();
		d.flatTrim();

		setTimeout(function () {
			console.log('connected to : ' + process.env.UUID);
			console.log('ready for flight');
			ACTIVE = true;
			app.listen(3000, function () {
				console.log('Example app listening on port 3000!');
			});
		}, 1000);
	});
});

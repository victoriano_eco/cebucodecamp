var server = 'http://10.11.15.110:3000';
$(function() {
	// init/end
	$('.btn-emergency').on('click', function() {
		SendCommand('/emergency');
	});

	$('.btn-takeoff').on('click', function() {
		SendCommand('/takeoff');
	});

	$('.btn-landing').on('click', function() {
		SendCommand('/landing');
	});

	// navigations
	$('.btn-up').on('click', function() {
		SendCommand('/up');
	});

	$('.btn-left').on('click', function() {
		SendCommand('/left');
	});

	$('.btn-right').on('click', function() {
		SendCommand('/right');
	});

	$('.btn-down').on('click', function() {
		SendCommand('/down');
	});

	// flight controls
	$('.btn-forward').on('click', function() {
		SendCommand('/forward');
	});

	$('.btn-tiltleft').on('click', function() {
		SendCommand('/tiltleft');
	});

	$('.btn-tiltright').on('click', function() {
		SendCommand('/tiltright');
	});

	$('.btn-backward').on('click', function() {
		SendCommand('/backward');
	});

	// flippers
	$('.btn-flip-front').on('click', function() {
		SendCommand('/flipfront');
	});

	$('.btn-flip-left').on('click', function() {
		SendCommand('/flipleft');
	});

	$('.btn-flip-right').on('click', function() {
		SendCommand('/flipright');
	});

	$('.btn-flip-back').on('click', function() {
		SendCommand('/flipback');
	});

});

function logger(msg) {
	$('.log-container').prepend(msg + '\n');
}

function SendCommand(command) {
	$.get(server + command, function(res) {
		logger(res);
	});
}
